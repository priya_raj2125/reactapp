import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Redirect } from "react-router";
import { PrivateRoute, PublicRoute } from "./_routes/PrivateRoute";
import Error from "./_component/error/Error";
import Login from "./_component/login/Login";
import Signup from "./_component/registration/Signup";
import Home from "./_component/home/Home";
import "../src/style.css";
class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    {/* <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <ul className="navbar-nav mr-auto">
                            <li>
                                <Link to={"/home"} className="nav-link">
                                    {" "}
                                    Home{" "}
                                </Link>
                            </li>
                            <li>
                                <Link to={"/login"} className="nav-link">
                                    Contact
                                </Link>
                            </li>
                            <li>
                                <Link to={"/signup"} className="nav-link">
                                    About
                                </Link>
                            </li>
                        </ul>
                    </nav>
                    <hr /> */}
                    <Switch>
                        <PublicRoute
                            restricted={true}
                            component={Login}
                            path="/"
                            exact
                        />
                        <PublicRoute
                            restricted={true}
                            component={Login}
                            path="/login"
                            exact
                        />
                        <PublicRoute
                            restricted={true}
                            component={Signup}
                            path="/signup"
                            exact
                        />
                        <PrivateRoute component={Home} path="/home" exact />
                        <Route path="/error" component={Error} />
                        <Redirect from="*" to="/error" />
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
