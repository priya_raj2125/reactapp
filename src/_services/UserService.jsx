// import axios from "axios";
import API from "./Api";
const apiUrl = process.env.REACT_APP_API_URL;
class UserService {
    async login(data) {
        try {
            // await axios({
            //     method: "POST",
            //     url: apiUrl + "/login",
            //     headers: {
            //         "content-type": "application/octet-stream",
            //     },
            //     data: data,
            // });
            const response = await API({
                method: "POST",
                url: apiUrl + "/login",
                data: data,
            });
            return response;
        } catch (error) {
            return error;
        }
    }
}

export default UserService;
