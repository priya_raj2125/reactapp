import React from "react";
import { Route, Redirect } from "react-router-dom";

export const PrivateRoute = ({ component: Component, ...rest }) => {
    let isLogin = JSON.parse(localStorage.getItem("loggedUser"));
    return (
        // Show the component only when the user is logged in
        // Otherwise, redirect the user to /login page
        <Route
            {...rest}
            render={(props) =>
                isLogin ? <Component {...props} /> : <Redirect to="/login" />
            }
        />
    );
};

export const PublicRoute = ({ component: Component, restricted, ...rest }) => {
    let isLogin = JSON.parse(localStorage.getItem("loggedUser"));
    return (
        // restricted = false meaning public route
        // restricted = true meaning restricted route
        <Route
            {...rest}
            render={(props) =>
                isLogin && restricted ? (
                    <Redirect to="/home" />
                ) : rest.path === "/" ? (
                    <Redirect to="/login" />
                ) : (
                    <Component {...props} />
                )
            }
        />
    );
};
