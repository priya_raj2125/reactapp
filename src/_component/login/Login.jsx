import React from "react";
import { Link } from "react-router-dom";
import * as constants from "../../constant";
import UserService from "../../_services/UserService";
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            errors: {},
            errorMsg: null,
        };
        this.state.constants = constants;
        this.userService = new UserService();
    }
    myChangeHandler = (event) => {
        // console.log(process.env.REACT_APP_WEB_URL);
        // console.log(process.env.NODE_ENV);
        // this.setState((prevState) => {
        //     let errors = Object.assign({}, prevState.errors);
        //     errors.username = "empty value";
        //     return { errors };
        // });
        // setTimeout(
        //     () =>
        //         this.setState((prevState) => {
        //             let errors = Object.assign({}, prevState.errors);
        //             errors = {};
        //             return { errors };
        //         }),
        //     1000
        // );
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({ [nam]: val });
    };
    loginSubmit = (event) => {
        event.preventDefault();
        if (this.handleValidation()) {
            var data = {
                username: this.state.username,
                password: this.state.password,
            };
            this.userService.login(data).then((res) => {
                if (res.status === 200) {
                    localStorage.setItem("loggedUser", JSON.stringify(data));
                    // this.props.history.push({
                    //     pathname: "/home",
                    //     state: { username: res.data.user.username },
                    // });
                    this.props.history.push("/home");
                } else {
                    this.setState({
                        errorMsg: this.state.constants.INVALID_LOGIN,
                    });
                    setTimeout(
                        () =>
                            this.setState({
                                errorMsg: null,
                            }),
                        3000
                    );
                }
            });
        }
    };
    handleValidation = () => {
        let fields = this.state;
        let formIsValid = true;

        //Username
        if (!fields["username"]) {
            formIsValid = false;
            this.state.errors["username"] = this.state.constants.REQUIRED_FIELD;
        }

        //Password
        if (!fields["password"]) {
            formIsValid = false;
            this.state.errors["password"] = this.state.constants.REQUIRED_FIELD;
        }

        // if (typeof fields["name"] !== "undefined") {
        //     if (!fields["name"].match(/^[a-zA-Z]+$/)) {
        //         formIsValid = false;
        //         errors["name"] = "Only letters";
        //     }
        // }

        // if (typeof fields["email"] !== "undefined") {
        //     let lastAtPos = fields["email"].lastIndexOf("@");
        //     let lastDotPos = fields["email"].lastIndexOf(".");

        //     if (
        //         !(
        //             lastAtPos < lastDotPos &&
        //             lastAtPos > 0 &&
        //             fields["email"].indexOf("@@") == -1 &&
        //             lastDotPos > 2 &&
        //             fields["email"].length - lastDotPos > 2
        //         )
        //     ) {
        //         formIsValid = false;
        //         errors["email"] = "Email is not valid";
        //     }
        // }

        this.setState({ errors: this.state.errors });
        return formIsValid;
    };
    render() {
        return (
            <div>
                <div className="container">
                    <h1 className="text-center">Login</h1>
                    {this.state.errorMsg && (
                        <label>{this.state.errorMsg}</label>
                    )}
                    <div className="row">
                        <form
                            className="login-form"
                            onSubmit={this.loginSubmit}
                            noValidate
                        >
                            <div className="form-group">
                                <label htmlFor="email">Email/Username:</label>
                                <input
                                    className="form-control mb-4"
                                    type="text"
                                    placeholder="Enter your email/username"
                                    name="username"
                                    value={this.state.username}
                                    onChange={this.myChangeHandler}
                                />
                                {this.state.errors &&
                                    this.state.errors.username && (
                                        <label>
                                            {
                                                this.state.constants
                                                    .REQUIRED_FIELD
                                            }
                                        </label>
                                    )}
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input
                                    className="form-control mb-4"
                                    type="password"
                                    placeholder="Enter your password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.myChangeHandler}
                                />
                                {this.state.errors &&
                                    this.state.errors.password && (
                                        <label>
                                            {
                                                this.state.constants
                                                    .REQUIRED_FIELD
                                            }
                                        </label>
                                    )}
                            </div>
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" className="mb-2" />{" "}
                                    Remember me
                                </label>
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Login
                            </button>
                            &nbsp;
                            <Link to="/signup">Sign up</Link>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
