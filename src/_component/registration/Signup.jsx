import React from "react";
import { Link } from "react-router-dom";
class Signup extends React.Component {
    render() {
        return (
            <div>
                <h2>Signup page</h2>
                <Link to="/login">Back to Sign in</Link>
            </div>
        );
    }
}

export default Signup;
