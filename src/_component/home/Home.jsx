import React from "react";
class Home extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props);
    }
    logout = () => {
        localStorage.removeItem("loggedUser");
        this.props.history.push("/login");
    };
    render() {
        return (
            <div>
                <h2>Hi,, {this.props.location.state}</h2>
                <button onClick={this.logout}>Logout</button>
            </div>
        );
    }
}

export default Home;
